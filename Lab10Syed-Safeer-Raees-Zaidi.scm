(define add 
(lambda(m n s z)
(m s (n s z))))

(define sub 
(lambda(m n)
(m pred n)))

(define and 
(lambda(m n a b)
(n (m a b)b)))

(define or 
(lambda(m n a b)
(n a (m a b))))

(define not 
(lambda(m a b)
(m b a)))

(define leq 
(lambda(m n)
(isZero (sub m n))))

(define geq 
(lambda(m n)
(OR (EQ(m n) NOT(LEQ(n m))))))
